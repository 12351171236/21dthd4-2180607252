# 21dthd4-2180607252
## Lê Đào Việt Anh
## Lớp: 21DTHD4
## MSSV: 2180607252
## Khoa: Công nghệ thông tin

| Title | Staff log in |
| --- | --- |
| Value Statement | As a staff, I want to log in to system Electronic Devices Management so that I manage product information |
| Acceptance Criteria | <ins>Acceptance Criteria:</ins><br/> The login data is entered by staff members, including username and password.<br/> Information used for staff logins is verified by the system.<br/> The system allows the personnel to access the system if the login information is accurate.<br/> The system will alert the personnel and request them to re-enter their login credentials if it is found to be invalid. |
| Definition of Done | Unit Tests Passed <br/> Acceptance Criteria Met <br/> Code Reviewed <br/> Funtional Tests Passed <br/> Non-Funtional Requirements Met <br/> Product Owner Accepts User Story |
| Owner | Mr Viet Anh |
| Iteration | Unscheduled |
| Estimate | |

![Staff log in](https://gitlab.com/12351171236/21dthd4-2180607252/-/raw/main/StaffLogIn.png?ref_type=heads)

|N| Req ID | Test Objected | Test Steps | Expected Result | Actual Result | Pass/Fail | Related Defects |
|-|--------|---------------|------------|-----------------|---------------|-----------|-----------------| 
|1| Req-01 | Log in successfully | 1. Go to ElectronicDevicesManagement application <br/> 2. Enter correct username and password <br/> 3. Click the "Log in" button | Display electronic device store management interface | | | |
|2| Req-02 | Log in failed when enter incorrect username or password | 1. Go to ElectronicDevicesManagement application <br/> 2. Enter incorrect username or password <br/> 3. Click the "Log in" button | Displays the error message "Username or password incorrect!" | | | |
|3| Req-03 | Log in failed without enter | 1. Go to ElectronicDevicesManagement application <br/> 2. Don't enter username or password <br/> 3. Click the "Log in" button | Displays the error message “Please enter full information!” | | | |


# 21dthd4-2180608611
## Do Dang Nhat Tan
## Lop: 21DTHD4
## MSSV: 2180608611
## Khoa: CNTT

| Title | Staff insert product information |
| --- | --- |
| Value Statement | As a staff, I want to add product information so that I can see the product infomation |
| Acceptance Criteria | <ins>Acceptance Criteria 1:</ins><br/>Show product information on the software when staff successfully adds product information </br> <br/><ins>Acceptance Criteria 2:</ins><br/>Display a warning message when a problem occurs while adding product information |
| Definition of Done | Unit Tests Passed <br/> Acceptance Criteria Met <br/> Code Reviewed <br/> Funtional Tests Passed <br/> Non-Funtional Requirements Met <br/> Product Owner Accepts User Story |
| Owner | Mr Tan |
| Iteration | Unscheduled |
| Estimate | |

![Staff insert product information](https://gitlab.com/12354721736/21dthd4-2180608611/-/raw/main/Staff%20insert%20product%20information.jpg?ref_type=heads)

|N| Req ID | Test Objected | Test Steps | Expected Result | Actual Result | Pass/Fail | Related Defects |
|-|--------|---------------|------------|-----------------|---------------|-----------|-----------------| 
|1| Req-01 | Add product information successfully | 1. Go to ElectronicDevicesManagement application <br/> 2. Log in as a staff member <br/> 3. Enter product information <br/> 4. Click the "Add" button | Show product with message "Add successfully!" and show information product | | | |
|2| Req-02 | Add product information failed when enter duplicate ID | 1. Go to ElectronicDevicesManagement application <br/> 2. Log in as a staff member <br/> 3. Enter duplicate ID with products before <br/> 4. Click the "Add" button | Displays the error message "This product ID existed!" | | | |
|3| Req-03 | Add product information failed without enter | 1. Go to ElectronicDevicesManagement application <br/> 2. Log in as a staff member <br/> 3. Don't enter product ID, name product, price, quantity, type product <br/> 4. Click the "Add" button | Displays the error message “Please enter full product information!” | | | |

# 21dthd4-2180608070
## Thái Hưng Thịnh
## Lớp: 21DTHD4
## MSSV: 2180608070
## Khoa: Công nghệ thông tin 

| Title | Staff updated product information |
| --- | --- |
| Value Statement | As a staff, I want to update product information so that I can edit product information |
| Acceptance Criteria | <ins>Acceptance Criteria :</ins><br/>Display a warning message when a problem occurs while updating product information</br> |
| Definition of Done | Unit Tests Passed <br/> Acceptance Criteria Met <br/> Code Reviewed <br/> Funtional Tests Passed <br/> Non-Funtional Requirements Met <br/> Product Owner Accepts User Story |
| Owner | Mr.Hthin |
| Iteration | Unscheduled |
| Estimate | |

![Staff updated product information](https://gitlab.com/thaihungthinh/21dthd4-2180608070/-/raw/main/%E1%BA%A2nh%20ch%E1%BB%A5p%20m%C3%A0n%20h%C3%ACnh%202023-10-13%20225012.png)

|N| Req ID | Test Objected | Test Steps | Expected Result | Actual Result | Pass/Fail | Related Defects |
|-|--------|---------------|------------|-----------------|---------------|-----------|-----------------| 
|1| Req-01 | Update product successfully | 1. Go to ElectronicDevicesManagement application <br/> 2. Log in as a staff member <br/> 3. Enter information product <br/> 4. Click the "Update" button | Show product with message "Update successfully" and show information product | | | |
|2| Req-02 | Update product failed when enter price of product is not number | 1. Go to ElectronicDevicesManagement application <br/> 2. Log in as a staff member <br/> 3. Enter price of product is not number <br/> 4. Click the "Update" button | Displays the error message "Please enter price of product is number!" | | | |
|3| Req-03 | Update product failed when enter quantity of product is not number | 1. Go to ElectronicDevicesManagement application <br/> 2. Log in as a staff member <br/> 3. Enter quantity of product is not number <br/> 4. Click the "Update" button | Displays the error message “Please enter quantity of product is number!” | | | |


# 21dthd4-2180608141
## Nguyễn Quốc Triệu
## Lớp: 21DTHD4
## MSSV: 2180608141
## Khoa: Công nghệ thông tin

| Title | Staff Delete Product Information |
| --- | --- |
| Value Statement | As a staff, I want to delete some products information so I can edit products information |
| Acceptance Criteria | <ins>Acceptance Criteria :</ins><br/>Display a warning message when a problem occurs while deleting product information</br> |
| Definition of Done | Unit Tests Passed <br/> Acceptance Criteria Met <br/> Code Reviewed <br/> Funtional Tests Passed <br/> Non-Funtional Requirements Met <br/> Product Owner Accepts User Story |
| Owner | Mr.Trieu |
| Interation | Unscheduled |
| Estimate | |

![Staff delete product information](https://gitlab.com/12351171236/21dthd4-2180608141/-/raw/main/Staff%20delete%20product%20information.png?ref_type=heads)

|N| Req ID | Test Objected | Test Steps | Expected Result | Actual Result | Pass/Fail | Related Defects |
|-|--------|---------------|------------|-----------------|---------------|-----------|-----------------| 
|1| Req-01 | Delete product information successfully when enter exist ID product | 1. Go to ElectronicDevicesManagement application <br/> 2. Log in as a staff member <br/> 3. Enter exist ID product  <br/> 4. Click the "Delete" button | Displays the message "Delete successfully" | | | |
|2| Req-02 | Delete product information successfully when choose to delete many prouct information | 1. Go to ElectronicDevicesManagement application <br/> 2. Log in as a staff member <br/> 3. Click checkbox "Choose to detele" <br/> 4. Click the "Delete" button | Displays the message "Delete successfully" | | | |
|3| Req-03 | Delete product information failed without choose any product information | 1. Go to ElectronicDevicesManagement application <br/> 2. Log in as a staff member <br/> 3. Don't choose any product information <br/> 4. Click the "Delete" button | Displays the error message “Delete failed” | | | |

# 21DTHD4-2180609148
## Phan Thành Trung
## Lớp 21DTHD4
## MSSV: 2180609148
## Khoa: Công nghệ thông tin

| Title | Staff search products information by name |
| --- | --- |
| Value Statement | As a staff, I want to search products information by name so that I can get product information |
| Acceptance Criteria | <ins>Acceptance Criteria:</ins><br/>Display a warning message when a problem occurs while searching product information |
| Definition of Done | Unit Tests Passed <br/> Acceptance Criteria Met <br/> Code Reviewed <br/> Funtional Tests Passed <br/> Non-Funtional Requirements Met <br/> Product Owner Accepts User Story |
| Owner | Mr.Trung |
| Interation | Unscheduled |
| Estimate | |

![Staff search products information by name](https://gitlab.com/12351865738/21DTHD4-2180609148/-/raw/main/Staff%20search%20products%20information%20by%20name.png?ref_type=heads)

|N| Req ID | Test Objected | Test Steps | Expected Result | Actual Result | Pass/Fail | Related Defects |
|-|--------|---------------|------------|-----------------|---------------|-----------|-----------------| 
|1| Req-01 | Searching successfully | 1. Go to ElectronicDevicesManagement application <br/> 2. Log in as a staff member <br/> 3. Enter name product into the search bar <br/> 4. Click the "Search" button | The product's information is displayed | | | |
|2| Req-02 | Searching non-existing product information by its name | 1. Go to ElectronicDevicesManagement application <br/> 2. Log in as a staff member <br/> 3. Enter name product not exist into the search bar <br/> 4. Click the "Search" button | No product information is displayed and displays the error message "Product not found!" | | | |
|3| Req-03 | Searching with an empty search query | 1. Go to ElectronicDevicesManagement application <br/> 2. Log in as a staff member <br/> 3. Don't enter name product on the search bar <br/> 4. Click the "Search" button | Displays the error message "Search failed!" | | | |
